This project is licensed under the GPLv2 license (see LICENSE).

--

## Getting Started

Please check the [subrepository's](https://bitbucket.org/stranger-games/lightning-fast-compilation-and-module-management-unity-sub/) readme.

Alternatively you can check the asset on the [Unity's asset store page](https://assetstore.unity.com/packages/slug/120668)
